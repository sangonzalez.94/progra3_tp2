package progra3_tp2.utilidades;

public class Textos {
	public static final String TITULO_APLICACION ="Trabajo Pr�ctico 2 - Regionalizaci�n por Arbol Generador M�nimo";
	
	public static final String CB_TXT_SELECCIONE_OPCION = "Seleccione una opci�n";
	
	public static final String LBL_INGRESE_VERTICE = "<html><body>Ingrese vertice (Ciudad, Provincia, Estado o Pa�s) <br> Tambi�n puede ingresarlo mediante fichero CSV respetando <br> vertice,latitud,longitud </body></html>";
	public static final String LBL_NOMBRE_VERTICE = "Nombre de vertice:";
	public static final String LBL_LATITUD = "Latitud:";
	public static final String LBL_LONGITUD = "Longitud:";
	public static final String LBL_CARGAR_FRONTERAS = "<html><body>Cree las fronteras o carguelas en CSV</body></html>";
	public static final String LBL_SIMILARIDAD ="Similaridad";
	public static final String LBL_CANTIDAD_REGIONES = "Cantidad de regiones";
	public static final String LBL_ALERT_NO_DEBEN_SER_MISMO_VERTICE =	"No deben ser el mismo v�rtice los elegidos";
	public static final String LBL_ALERT_INGRESAR_SIMILARIDAD = "Debes ingresar la similaridad";
	public static final String BTN_AGREGAR_VERTICE =  "Agregar Vertice";
	public static final String BTN_SUBIR_CSV = "Subir CSV";
	public static final String BTN_AGREGAR_FRONTERA = "Agregar frontera";
	public static final String BTN_VER_MAPA_ACTUAL = "Ver mapa actual";
	public static final String BTN_LANZAR_REGIONALIZACION = "Lanzar regionalizaci�n";
	
}
