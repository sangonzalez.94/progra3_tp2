package progra3_tp2.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;



import progra3_tp2.model.ComboModel;
import progra3_tp2.model.TableModel;
import progra3_tp2.model.grafo.mapa.AlgoritmoDePrim;
import progra3_tp2.model.grafo.mapa.Arista;
import progra3_tp2.model.grafo.mapa.BFS;
import progra3_tp2.model.grafo.mapa.Grafo;
import progra3_tp2.model.grafo.mapa.Vertice;

public class LauncherController {
	
	private TableModel tableModel;
	private ComboModel comboModel1;
	private ComboModel comboModel2;
	
	private Map<String, Vertice> mapStringVertice;
	private ArrayList<Arista> fronteras;
	private ArrayList<Vertice> provincias;
	
	private Grafo grafoMapaOriginal;
	private Grafo grafo;
	private Grafo grafoMinimo;
	
	public LauncherController() {
		provincias = new ArrayList<>();
		fronteras = new ArrayList<>();
		mapStringVertice = new HashMap<>();

	}
	public void crearNuevosModel() {
		comboModel1 = new ComboModel();
		comboModel2 = new ComboModel();
		tableModel = new TableModel();
		tableModel.addColumn("ig");
		tableModel.addColumn("Frontera");
		tableModel.addColumn("Similaridad");
	}
	
	public TableModel getTableModel() {
		return tableModel;
	}
	
	public ComboModel getComboModel1() {
		return this.comboModel1;
	}
	
	public ComboModel getComboModel2() {
		return this.comboModel2;
	}
	
	public void anadirElementoAlComboModel1(Vertice vertice) {
		this.comboModel1.addElement(vertice);
	}
	
	public void anadirElementoAlComboModel2(Vertice vertice) {
		this.comboModel2.addElement(vertice);
	}
	
	public List<Vertice> getVerticesArbolGeneradorMinimo() {
		grafo = new Grafo();
		for(Vertice vertice:provincias) {
			grafo.agregarVertice(vertice);
		}

		for(Arista arista:fronteras) {
			grafo.agregarArista(arista.getVerticeA(), arista.getVerticeB(), arista.getSimilaridad());
		}

		if(!BFS.esConexo(grafo)) {
			throw new IllegalArgumentException("El grafo no es conexo");
		}

		grafoMinimo = AlgoritmoDePrim.obtenerArbolGeneradorMinimo(grafo);

		List<Vertice> verticesGM = grafoMinimo.getVertices();
		return verticesGM;
	}
	public Map<String, Vertice> getMapStringVertice() {
		return mapStringVertice;
	}
	public void setMapStringVertice(Map<String, Vertice> mapStringVertice) {
		this.mapStringVertice = mapStringVertice;
	}
	public ArrayList<Arista> getFronteras() {
		return fronteras;
	}
	public void setFronteras(ArrayList<Arista> fronteras) {
		this.fronteras = fronteras;
	}
	public ArrayList<Vertice> getProvincias() {
		return provincias;
	}
	public void setProvincias(ArrayList<Vertice> provincias) {
		this.provincias = provincias;
	}
	
	public Grafo getGrafoMinimo() {
		return this.grafoMinimo;
	}
	public void agregarFrontera(Vertice vertice, Vertice vertice2, double parseDouble) {
		Arista a = new Arista(vertice, vertice2, parseDouble);
		this.fronteras.add(a);
		this.tableModel.addRow(a.getDataEnArregloDeString());
	}
	public void addVertice(String text, double parseDouble, double parseDouble2) {
		Vertice v = new Vertice(text, parseDouble, parseDouble2);
		this.provincias.add(v);
		this.getMapStringVertice().put(text, v);
		this.anadirElementoAlComboModel1(v);
		this.anadirElementoAlComboModel2(v);
	}
	public Grafo getGrafoMapaOriginal() {
		return grafoMapaOriginal;
	}
	public void setGrafoMapaOriginal(Grafo grafo) {
		this.grafoMapaOriginal = grafo;
	}
	
	public List<Vertice> getVerticesGrafoMapaOriginal() {
		grafoMapaOriginal = new Grafo();
		for(Vertice vertice:provincias) {
			grafoMapaOriginal.agregarVertice(vertice);
		}
		for(Arista arista:fronteras) {
			grafoMapaOriginal.agregarArista(arista.getVerticeA(), arista.getVerticeB(), arista.getSimilaridad());
		}
		if(!BFS.esConexo(grafoMapaOriginal)) {
			throw new IllegalArgumentException("El grafo no es conexo");
		}

		List<Vertice> verticesGM = grafoMapaOriginal.getVertices();
		return verticesGM;
	}
	
}
