package progra3_tp2.view;

import static progra3_tp2.utilidades.Textos.*;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.UIManager;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;

import progra3_tp2.controller.LauncherController;
import progra3_tp2.model.grafo.mapa.Arista;
import progra3_tp2.model.grafo.mapa.Vertice;

public class Launcher implements ActionListener {
	
	//Swing
	private JFrame frame;
	private JPanel panelIngresoDeDatos;
	private JButton btnVerMapaActual, btnLimpiarPantalla, btnLanzarRegionalizacion, btnAgregarVertice,btnAgregarFronteras;
	private JTextField tfNombreVertice;
	private JTextField tfLatitud;
	private JTextField tfLongitud;
	private JLabel lblCrearFronteras;
	private JTextField tfSimilaridad;
	private JTable tblFronterasSimilaridad;
	private JLabel JLNombreVertice;
	private JComboBox<Vertice> cbVertice1, cbVertice2;
	private JMapViewer mapRegiones, mapOriginal;
	
	private LauncherController launcherController;
	
	

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Launcher window = new Launcher();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Launcher() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		
		/**/
		frame = new JFrame();
		frame.setBounds(100, 100, 770, 768);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle(TITULO_APLICACION);
		try{
			JFrame.setDefaultLookAndFeelDecorated(true);
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
		}catch (Exception e){
		  e.printStackTrace();
		}
		launcherController = new LauncherController();
		generarPanelesDeLaPantalla();
		generarElementosDePantallaCargarInformacion();
	}

	private void generarPanelesDeLaPantalla() {
		panelIngresoDeDatos = new JPanel();
		panelIngresoDeDatos.setBounds(0, 0, 770, 768);
		panelIngresoDeDatos.setLayout(null);
		panelIngresoDeDatos.setVisible(true);
		frame.getContentPane().add(panelIngresoDeDatos);
	}

	private void generarElementosDePantallaCargarInformacion() {
		
		mapOriginal = new JMapViewer();
		mapOriginal.setBounds(400, 55, 300, 300);
		panelIngresoDeDatos.add(mapOriginal);
		
		JLabel lblIngreseVertice = new JLabel(LBL_INGRESE_VERTICE);
		lblIngreseVertice.setBounds(20, 11, 331, 83);
		panelIngresoDeDatos.add(lblIngreseVertice);
	
		/*Nombre de vertice*/
		JLNombreVertice = new JLabel(LBL_NOMBRE_VERTICE);
		JLNombreVertice.setBounds(30, 99, 140, 14);
		panelIngresoDeDatos.add(JLNombreVertice);
		
		tfNombreVertice = new JTextField();
		tfNombreVertice.setBounds(179, 99, 182, 20);
		panelIngresoDeDatos.add(tfNombreVertice);
		tfNombreVertice.setColumns(10);
		/*Fin de nombre de vertice*/
		
		/* Inicio Latitud */
		JLabel lblLatitud = new JLabel(LBL_LATITUD);
		lblLatitud.setBounds(30, 119, 140, 14);
		panelIngresoDeDatos.add(lblLatitud);
		
		tfLatitud = new JTextField();
		tfLatitud.setColumns(10);
		tfLatitud.setBounds(179, 119, 182, 20);
		panelIngresoDeDatos.add(tfLatitud);
		/*Fin latitud*/
		
		/*Inicio longitud*/
		JLabel lblLongitud = new JLabel(LBL_LONGITUD);
		lblLongitud.setBounds(30, 139, 140, 14);
		panelIngresoDeDatos.add(lblLongitud);
		
		tfLongitud = new JTextField();
		tfLongitud.setColumns(10);
		tfLongitud.setBounds(179, 139, 182, 20);
		panelIngresoDeDatos.add(tfLongitud);
		/*Fin longitud*/
		
		/*Boton de agregar vertice*/
		btnAgregarVertice = new JButton(BTN_AGREGAR_VERTICE);
		btnAgregarVertice.setBounds(30, 164, 149, 23);
		btnAgregarVertice.addActionListener(this);
		panelIngresoDeDatos.add(btnAgregarVertice);
		/*Fin boton agregar vertice*/
		
		
		lblCrearFronteras = new JLabel(LBL_CARGAR_FRONTERAS);
		lblCrearFronteras.setBounds(20, 198, 331, 83);
		panelIngresoDeDatos.add(lblCrearFronteras);
		
		
		
		/*COMBOS*/
	
		cbVertice1 = new JComboBox<>();
		cbVertice1.setBounds(30, 303, 159, 22);
		panelIngresoDeDatos.add(cbVertice1);
		
		cbVertice2 = new JComboBox<>();
		cbVertice2.setBounds(202, 303, 159, 22);
		panelIngresoDeDatos.add(cbVertice2);
		/*FIN TRATAMIENTO DE COMBOS*/
		
		JLabel lblSimilaridad = new JLabel(LBL_SIMILARIDAD);
		lblSimilaridad.setBounds(30, 340, 140, 14);
		panelIngresoDeDatos.add(lblSimilaridad);
		
		tfSimilaridad = new JTextField();
		tfSimilaridad.setColumns(10);
		tfSimilaridad.setBounds(179, 336, 182, 20);
		panelIngresoDeDatos.add(tfSimilaridad);
		
		btnAgregarFronteras = new JButton(BTN_AGREGAR_FRONTERA);
		btnAgregarFronteras.addActionListener(this);
		btnAgregarFronteras.setBounds(30, 361, 149, 23);
		panelIngresoDeDatos.add(btnAgregarFronteras);
	
		tblFronterasSimilaridad = new JTable();
		tblFronterasSimilaridad.setEnabled(false);
		tblFronterasSimilaridad.setBounds(25, 418, 336, 253);
		panelIngresoDeDatos.add(tblFronterasSimilaridad);
		
		btnVerMapaActual = new JButton(BTN_VER_MAPA_ACTUAL);
		btnVerMapaActual.addActionListener(this);
		btnVerMapaActual.setBounds(381, 25, 331, 23);
		btnVerMapaActual.addActionListener(this);
		panelIngresoDeDatos.add(btnVerMapaActual);
		
		btnLanzarRegionalizacion = new JButton(BTN_LANZAR_REGIONALIZACION);
		btnLanzarRegionalizacion.addActionListener(this);
		btnLanzarRegionalizacion.setBounds(381, 361, 331, 23);
		panelIngresoDeDatos.add(btnLanzarRegionalizacion);
		
		mapRegiones = new JMapViewer();
		mapRegiones.setBounds(400, 395, 300, 300);
		panelIngresoDeDatos.add(mapRegiones);
		
		btnLimpiarPantalla = new JButton("Limpiar pantalla");
		btnLimpiarPantalla.addActionListener(this);
		btnLimpiarPantalla.setBounds(35, 685, 91, 23);
		panelIngresoDeDatos.add(btnLimpiarPantalla);
	
		launcherController.crearNuevosModel();
		this.cargarModel();
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource()==btnAgregarVertice) {
			accionAgregarVertice();
        }else if(e.getSource()==btnAgregarFronteras) {
			accionAgregarFronteras();
		}else if(e.getSource()==btnLanzarRegionalizacion) {
			accionLanzarRegionalizacion();
		}else if(e.getSource()==btnVerMapaActual) {
			accionMapaActual();
		}else if(e.getSource()==btnLimpiarPantalla) {
			accionLimpiarPantalla();
		}
		
		
	}

	private void accionLanzarRegionalizacion() {
		for(Vertice verticeGM: launcherController.getVerticesArbolGeneradorMinimo()) {
			MapMarker marker = new MapMarkerDot(verticeGM.getNombre(),new Coordinate(verticeGM.getLatitud(), verticeGM.getLongitud()));
			mapRegiones.addMapMarker(marker);
			for(Arista aristaGM : launcherController.getGrafoMinimo().aristasDeUnVertice(verticeGM)) {
				Coordinate one = new Coordinate(aristaGM.getVerticeA().getLatitud(),aristaGM.getVerticeA().getLongitud());
				Coordinate two = new Coordinate(aristaGM.getVerticeB().getLatitud(),aristaGM.getVerticeB().getLongitud());
				List<Coordinate> route = new ArrayList<Coordinate>(Arrays.asList(one, two, two));
				mapRegiones.addMapPolygon(new MapPolygonImpl(route));
				mapRegiones.validate();
			}
		}
	}
	private void accionLimpiarPantalla() {
		int respuesta = JOptionPane.showConfirmDialog(panelIngresoDeDatos, "�Desea limpiar la pantalla?", "LIMPIAR PANTALLA", JOptionPane.YES_NO_OPTION);
		if(respuesta==JOptionPane.YES_OPTION) {
			launcherController.crearNuevosModel();
			cargarModel();
			limpiarTextFields();
		}
	}
	
	private void cargarModel() {
		cbVertice1.setModel(launcherController.getComboModel1());
		cbVertice2.setModel(launcherController.getComboModel2());
		tblFronterasSimilaridad.setModel(launcherController.getTableModel());
	}

	private void accionMapaActual() {
		List<Vertice> verticesGM =launcherController.getVerticesGrafoMapaOriginal();
		for(Vertice verticeGM: verticesGM) {
			MapMarker marker = new MapMarkerDot(verticeGM.getNombre(),new Coordinate(verticeGM.getLatitud(), verticeGM.getLongitud()));
			mapOriginal.addMapMarker(marker);
			for(Arista aristaGM : launcherController.getGrafoMapaOriginal().aristasDeUnVertice(verticeGM)) {
				Coordinate one = new Coordinate(aristaGM.getVerticeA().getLatitud(),aristaGM.getVerticeA().getLongitud());
				Coordinate two = new Coordinate(aristaGM.getVerticeB().getLatitud(),aristaGM.getVerticeB().getLongitud());
				List<Coordinate> route = new ArrayList<Coordinate>(Arrays.asList(one, two, two));
				mapOriginal.addMapPolygon(new MapPolygonImpl(route));
				mapOriginal.validate();
			}
		}
	}

	private void accionAgregarFronteras() {
		if(cbVertice1.getSelectedItem().toString().equals(cbVertice2.getSelectedItem().toString())) {
			JOptionPane.showMessageDialog(panelIngresoDeDatos, LBL_ALERT_NO_DEBEN_SER_MISMO_VERTICE );
		}else if(tfSimilaridad == null || tfSimilaridad.getText().isEmpty()) {
			JOptionPane.showMessageDialog(panelIngresoDeDatos, LBL_ALERT_INGRESAR_SIMILARIDAD);
		}else {
			launcherController.agregarFrontera(launcherController.getMapStringVertice().get(cbVertice1.getSelectedItem().toString()), 
					launcherController.getMapStringVertice().get(cbVertice2.getSelectedItem().toString()),
					Double.parseDouble(tfSimilaridad.getText()));
			tblFronterasSimilaridad.revalidate();
		}
	}

	private void accionAgregarVertice() {
		String mensajesDeError = "";
		if(!verificarTfField(tfNombreVertice)) {
			mensajesDeError = mensajesDeError + ("Deb�s ingresar un nombre\n");
		}
		if(verificarTfField(tfLatitud) 
				&& verificarTfField(tfLongitud)
				&& verificarTfFieldNumero(tfLatitud)
				&& verificarTfFieldNumero(tfLongitud)) {
			launcherController.addVertice(tfNombreVertice.getText(),Double.parseDouble(tfLatitud.getText()),Double.parseDouble(tfLongitud.getText()) );//getProvincias().add(vertice);
			limpiarTextFields();
			btnVerMapaActual.transferFocus();
		}else {
			mensajesDeError = mensajesDeError + ("Debes ingresar latitud y longitud. Ambos deben ser n�meros. \n");
		}
		if(!mensajesDeError.isEmpty()) {
			
			JOptionPane.showMessageDialog(panelIngresoDeDatos, mensajesDeError);
		}
	}

	private boolean verificarTfFieldNumero(JTextField tf) {
		if(tf == null || tf.getText().isEmpty()) {
			return false;
		}
		try {
			Double.parseDouble(tf.getText());
			return true;
		}catch (Exception e) {
			return false;
		}
	}

	private boolean verificarTfField(JTextField tf) {
		if(tf == null || tf.getText().isEmpty()) {
			return false;
		}
		return true;
		
	}
	
	
	private void limpiarTextFields() {
		tfLatitud.setText("");
		tfLongitud.setText("");
		tfNombreVertice.setText("");
		tfSimilaridad.setText("");	}
}
