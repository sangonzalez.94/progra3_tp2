package progra3_tp2.model.grafo.mapa;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * El grafo
 * @author Santiago Gonzalez
 *
 */
public class Grafo {
	private List<Vertice> vertices; //provincias, ciudades, etc.
	private List<Arista> aristas;
	private Map<Vertice, Set<Vertice>> vecinos;
	private Map<Vertice, Set<Arista>> aristasPorVertice;
	
	public Grafo () {
		vertices = new ArrayList<>();
		aristas = new ArrayList<>();
		vecinos = new HashMap<>();
		aristasPorVertice = new HashMap<>();
	}
	
	
	public void agregarVertice(Vertice vertice) {
		if(!vertices.contains(vertice)) {
		vertices.add(vertice);}
	}
	
	public void agregarVertice(Vertice ... arreglo) {
		
		for(Vertice vertice:arreglo){
			vertices.add(vertice);
		}
		
		
	}
	
	public List<Vertice> getVertices(){
		return vertices;
	}

	public Vertice getVertice(int i) {
		return vertices.get(i);
	}

	/**
	 * Devuelve la cantidad de v�rtices del grafo
	 * @return int con la cantidad de vertices
	 */
	public int tamano() {
		return vertices.size();
	}
	/*
	public void agregarArista(Arista arista) {
		
	}*/
	
	public void agregarArista (Vertice verticeA, Vertice verticeB, Double similaridad) {
		Arista arista = new Arista(verticeA, verticeB, similaridad);
		this.aristas.add(arista);
		if(vecinos.containsKey(verticeA)) {
			vecinos.get(verticeA).add(verticeB);
		}else {
			Set<Vertice> verticesSet = new HashSet<>();
			verticesSet.add(verticeB);
			vecinos.put(verticeA, verticesSet);
		}
		if(vecinos.containsKey(verticeB)) {
			vecinos.get(verticeB).add(verticeA);
		}else {
			Set<Vertice> verticesSet = new HashSet<>();
			verticesSet.add(verticeA);
			vecinos.put(verticeB, verticesSet);
		}
		/*Agrego la arista a un mapa para relacionarla con los vertices*/
		if(aristasPorVertice.containsKey(verticeA)) {
			aristasPorVertice.get(verticeA).add(arista);
		}else {
			Set<Arista> setAristas = new HashSet<>();
			setAristas.add(arista);
			aristasPorVertice.put(verticeA, setAristas);
		}
		
		if(aristasPorVertice.containsKey(verticeB)) {
			aristasPorVertice.get(verticeB).add(arista);
		}else {
			Set<Arista> setAristas = new HashSet<>();
			setAristas.add(arista);
			aristasPorVertice.put(verticeB, setAristas);
		}
		
	}
	
	public Set<Vertice> vecinosDeVertice (Vertice v){
		return vecinos.get(v);
	}


	public void eliminarVertice(Vertice vertice) {
		if(vertices.contains(vertice)) {
			vertices.remove(vertice);
		}
		
		if(vecinos.containsKey(vertice)) {
			vecinos.remove(vertice);
		}
		
		List<Arista> aristaParaRemover = new ArrayList<>();
		for(Arista arista:aristas) {
			if(arista.getVerticeA().equals(vertice)||arista.getVerticeB().equals(vertice)) {
				aristaParaRemover.add(arista);
			}
		}
		
		aristas.removeAll(aristaParaRemover);
		
		//Remover de vecinos
		vecinos.remove(vertice);
		for (Map.Entry<Vertice, Set<Vertice>> entry : vecinos.entrySet()) {
			entry.getValue().remove(vertice);
		}

	}
	
	public int ubicacionDelVertice(Vertice v) {
		return vertices.indexOf(v);
	}
	
	public Set<Arista> aristasDeUnVertice (Vertice v){
		return aristasPorVertice.get(v);
	}
	
	protected List<Arista> getAristas() {
		return this.aristas;
	}
	
	protected Map<Vertice, Set<Vertice>> getVecinos(){
		return vecinos;
	}
	
	
	protected Map<Vertice, Set<Arista>> getAristasPorVertice(){
		return aristasPorVertice;
	}
	
	
	
}
