package progra3_tp2.model.grafo.mapa;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class BFS {
	
	private static List<Vertice> verticesRecorridos;
	private static boolean [] marcados;

	
	
	
	public static boolean esConexo(Grafo grafo) {
		return alcanzablesDesdeUnVertice(grafo, grafo.getVertice(0)).size()==grafo.tamano();
		
	}
	
	public static Set<Vertice> alcanzablesDesdeUnVertice(Grafo grafo, Vertice v) {
		Set<Vertice> ret = new HashSet<>();
		verticesRecorridos = new ArrayList<Vertice>();
		verticesRecorridos.add(v);
		marcados = new boolean [grafo.tamano()];
		
		while(verticesRecorridos.size()>0) {
			Vertice i=verticesRecorridos.get(0);
			marcados[grafo.ubicacionDelVertice(i)]=true;
			ret.add(i);
			
			anadirPendientes(grafo, i);
			verticesRecorridos.remove(0);
			
		}
		return ret;


	}
	
	private static void anadirPendientes(Grafo g, Vertice i) {

		for (Vertice vertice : g.vecinosDeVertice(i)) {
			if (marcados[g.ubicacionDelVertice(vertice)] == false && !verticesRecorridos.contains(vertice)) {
				verticesRecorridos.add(vertice);
			}
		}	
	}

}
