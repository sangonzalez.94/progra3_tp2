package progra3_tp2.model.grafo.mapa;

public class Arista {
	//Entre las dos vertices que hay una arista o una similaridad.
		private Vertice verticeA; 
		private Vertice verticeB; 
		
		private Double similaridad;

		public Vertice getVerticeA() {
			return verticeA;
		}

		public void setVerticeA(Vertice verticaA) {
			this.verticeA = verticaA;
		}

		public Vertice getVerticeB() {
			return verticeB;
		}

		public void setVerticeB(Vertice verticeB) {
			this.verticeB = verticeB;
		}

		public Double getSimilaridad() {
			return similaridad;
		}

		public void setSimilaridad(Double similaridad) {
			verificarSimilaridad(similaridad);
			this.similaridad = similaridad;
		}

		public Arista() {
			super();
		}
		
		public Arista(Vertice verticaA, Vertice verticeB, Double similaridad) {
			super();
			this.verticeA = verticaA;
			this.verticeB = verticeB;
			verificarSimilaridad(similaridad);
			this.similaridad = similaridad;
		}
		
		/**
		 * Se verifica las condiciones de la similaridad.
		 * La similaridad debe ser mayor a 0 no ser nula. En ese caso lanza una excepci�n.
		 * @param similaridad
		 */
		private void verificarSimilaridad(Double similaridad) {
			if(similaridad == null) {
				throw new IllegalArgumentException("La similaridad no debe ser nula.");
			}
			if(similaridad < 0) {
				throw new IllegalArgumentException("La similaridad debe ser mayor que 0");
			}
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((similaridad == null) ? 0 : similaridad.hashCode());
			result = prime * result + ((verticeA == null) ? 0 : verticeA.hashCode());
			result = prime * result + ((verticeB == null) ? 0 : verticeB.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Arista other = (Arista) obj;
			if (similaridad == null) {
				if (other.similaridad != null)
					return false;
			} else if (!similaridad.equals(other.similaridad))
				return false;
			if (verticeA == null) {
				if (other.verticeA != null)
					return false;
			} else if (!verticeA.equals(other.verticeA))
				return false;
			if (verticeB == null) {
				if (other.verticeB != null)
					return false;
			} else if (!verticeB.equals(other.verticeB))
				return false;
			return true;
		}
		
		public String[] getDataEnArregloDeString() {
			return new String[] { verticeA.getNombre() , verticeB.getNombre(), Double.toString(similaridad)};
		}
		
}
