package progra3_tp2.model.grafo.mapa;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AlgoritmoDePrim {
	
	public static Grafo obtenerArbolGeneradorMinimo(Grafo grafo) {
		Grafo grafoRet = new Grafo();
		
		//Variables
		List<Vertice> verticesDelGrafo = grafo.getVertices();
		int vertices = grafo.tamano();
		int aristasEsperadas = vertices-1;
		List<Vertice> verticesAgregadas = new ArrayList<>(); //vertices agregadas. Si esta ac� no debo visitarla de nuevo
		Set<Arista> aristasAgregadas= new HashSet<>(); 
		int aristas = 1;
		verticesAgregadas.add(verticesDelGrafo.get(0));
		
		while (aristas <= aristasEsperadas) {
			Arista aristaMenorPeso = null;
			Vertice nuevoVerticeQueUne = null;
			for(Vertice vertice: verticesAgregadas) {
				Set<Arista> aristasDeUnVertice = grafo.aristasDeUnVertice(vertice);
				for (Arista aristaDeVertice : aristasDeUnVertice) {
					if(aristaMenorPeso == null) {
						if(nuevoVerticeQueUne == null) {
							if(!verticesAgregadas.contains(aristaDeVertice.getVerticeA())){
								aristaMenorPeso = aristaDeVertice;
								nuevoVerticeQueUne = aristaDeVertice.getVerticeA();
							}else if(!verticesAgregadas.contains(aristaDeVertice.getVerticeB())) {
								aristaMenorPeso = aristaDeVertice;
								nuevoVerticeQueUne = aristaDeVertice.getVerticeB();
							}
						}
					}else if(aristaMenorPeso.getSimilaridad()>aristaDeVertice.getSimilaridad()) {
						if(!verticesAgregadas.contains(aristaDeVertice.getVerticeA())){
							aristaMenorPeso = aristaDeVertice;
							nuevoVerticeQueUne = aristaDeVertice.getVerticeA();
						}else if(!verticesAgregadas.contains(aristaDeVertice.getVerticeB())) {
							aristaMenorPeso = aristaDeVertice;
							nuevoVerticeQueUne = aristaDeVertice.getVerticeB();
						}
					}
				}
			}
			verticesAgregadas.add(nuevoVerticeQueUne);
			aristasAgregadas.add(aristaMenorPeso);
			aristas++;
		}
		//agrego los vertices al grafo
		for(Vertice verticeA: verticesAgregadas) {
			grafoRet.agregarVertice(verticeA);
		}
		
		for(Arista aristaA:aristasAgregadas) {
			grafoRet.agregarArista(aristaA.getVerticeA(),aristaA.getVerticeB(),aristaA.getSimilaridad());
		}
		return grafoRet;
		
	}
}
