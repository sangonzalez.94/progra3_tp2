package src.test.progra3_tp2.grafo.mapa;
import static org.junit.Assert.*;


import org.junit.Test;

import progra3_tp2.model.grafo.mapa.Grafo;
import progra3_tp2.model.grafo.mapa.Vertice;
public class GrafoTests {

	
	@Test
	public void crearGrafoYAnadirUnVertice() {
		Grafo grafo = new Grafo();
		Vertice v = new Vertice("Buenos Aires");
		grafo.agregarVertice(v);
		assertTrue("El grafo contiene un elemento", grafo.tamano()==1);
	}
	
	@Test
	public void crearGrafoYAnadirDosVerticesMismoNombreDistintoObjeto() {
		Grafo grafo = new Grafo();
		Vertice v = new Vertice("Buenos Aires");
		Vertice v2 = new Vertice("Buenos Aires");
		grafo.agregarVertice(v);
		grafo.agregarVertice(v2);
		assertTrue("El grafo contiene un elemento", grafo.tamano()==1);
	}
	
	@Test
	public void crearGrafoYAnadirDosVerticesDistintoNombre() {
		Grafo grafo = new Grafo();
		Vertice v = new Vertice("Buenos Aires");
		Vertice v2 = new Vertice("Santa Fe");
		grafo.agregarVertice(v);
		grafo.agregarVertice(v2);
		assertTrue("El grafo contiene dos elemento", grafo.tamano()==2);
		assertTrue(grafo.getVertices().size()==2);
	}
	
	
	@Test
	public void eliminarDosVerticesLimitrofesConUna() {
		//Primero seteo los vertices
		Vertice buenosAires,caba,cordoba,santaFe,laPampa,rioNegro,chubut,santaCruz;
		buenosAires = new Vertice("Buenos Aires");
		caba = new Vertice("CABA");
		cordoba = new Vertice("Cordoba");
		santaFe = new Vertice("Santa Fe");
		laPampa = new Vertice("La Pampa");
		rioNegro = new Vertice("Rio Negro");
		chubut = new Vertice("Chubut");
		santaCruz = new Vertice("Santa Cruz");
		//Segundo: armo el grafo
		Grafo grafo = new Grafo();
		
		//tercero agrego las vertices al grafo
		grafo.agregarVertice(buenosAires, caba, cordoba, santaFe, laPampa, rioNegro, chubut, santaCruz);
		
		//cuarto agrego las provincias limitrofes con sus respectivas similaridades
		grafo.agregarArista(buenosAires, caba, 0.2);
		grafo.agregarArista(buenosAires, cordoba, 0.5);
		grafo.agregarArista(buenosAires, santaFe, 0.5);
		grafo.agregarArista(buenosAires, rioNegro, 1.0);
		grafo.agregarArista(cordoba, santaFe, 0.5);
		grafo.agregarArista(laPampa, buenosAires, 1.5);
		grafo.agregarArista(laPampa, cordoba, 1.5);
		grafo.agregarArista(rioNegro, laPampa, 2.0);
		grafo.agregarArista(santaCruz, rioNegro, 0.5);
		grafo.agregarArista(santaCruz, chubut, 0.5);
		grafo.eliminarVertice(cordoba);
		grafo.eliminarVertice(laPampa);
		//verifico los vecinos
		assertTrue(grafo.vecinosDeVertice(buenosAires).size()==3);
		
	}
	/**
	 * No son test unitarios. Simulan ser tests de uso real
	 */
	@Test
	public void casoPruebaSimilarReal() {
		//Primero seteo los vertices
		Vertice buenosAires,caba,cordoba,santaFe,laPampa,rioNegro,chubut,santaCruz;
		buenosAires = new Vertice("Buenos Aires");
		caba = new Vertice("CABA");
		cordoba = new Vertice("Cordoba");
		santaFe = new Vertice("Santa Fe");
		laPampa = new Vertice("La Pampa");
		rioNegro = new Vertice("Rio Negro");
		chubut = new Vertice("Chubut");
		santaCruz = new Vertice("Santa Cruz");
		//Segundo: armo el grafo
		Grafo grafo = new Grafo();
		
		//tercero agrego las vertices al grafo
		grafo.agregarVertice(buenosAires, caba, cordoba, santaFe, laPampa, rioNegro, chubut, santaCruz);
		
		//cuarto agrego las provincias limitrofes con sus respectivas similaridades
		grafo.agregarArista(buenosAires, caba, 0.2);
		grafo.agregarArista(buenosAires, cordoba, 0.5);
		grafo.agregarArista(buenosAires, santaFe, 0.5);
		grafo.agregarArista(buenosAires, rioNegro, 1.0);
		grafo.agregarArista(cordoba, santaFe, 0.5);
		grafo.agregarArista(laPampa, buenosAires, 1.5);
		grafo.agregarArista(laPampa, cordoba, 1.5);
		grafo.agregarArista(rioNegro, laPampa, 2.0);
		grafo.agregarArista(santaCruz, rioNegro, 0.5);
		grafo.agregarArista(santaCruz, chubut, 0.5);
		
		//verifico los vecinos
		assertTrue(grafo.vecinosDeVertice(buenosAires).size()==5);
		
	}
	

	@Test
	public void crearGrafoYAnadirDosVerticesMismoNombreConDistintasMayusculas() {
		Grafo grafo = new Grafo();
		Vertice v = new Vertice("Buenos Aires");
		Vertice v2 = new Vertice("Buenos aires");
		grafo.agregarVertice(v);
		grafo.agregarVertice(v2);
		assertTrue("El grafo contiene un elemento", grafo.tamano()==1);
	}
}
