package src.test.progra3_tp2.grafo.mapa;
import static org.junit.Assert.*;

import java.util.Set;

import org.junit.Test;

import progra3_tp2.model.grafo.mapa.BFS;
import progra3_tp2.model.grafo.mapa.Grafo;
import progra3_tp2.model.grafo.mapa.Vertice;

public class BFSTests {
	
	@Test
	public void grafoEsConexo() {
		Grafo grafo = new Grafo();
		Vertice v1 = new Vertice("Buenos Aires");
		Vertice v2 = new Vertice("Buenos Aires Ciudad");
		grafo.agregarVertice(v1,v2);
		
		grafo.agregarArista(v1, v2, 0.2);
		
		assertTrue(BFS.esConexo(grafo));
	}
	
	@Test
	public void verticesAlcanzadasDesdeUnGrafo() {
		// Primero seteo los vertices
		Vertice buenosAires, caba, cordoba, santaFe, laPampa, rioNegro, chubut, santaCruz;
		buenosAires = new Vertice("Buenos Aires");
		caba = new Vertice("CABA");
		cordoba = new Vertice("Cordoba");
		santaFe = new Vertice("Santa Fe");
		laPampa = new Vertice("La Pampa");
		rioNegro = new Vertice("Rio Negro");
		chubut = new Vertice("Chubut");
		santaCruz = new Vertice("Santa Cruz");
		// Segundo: armo el grafo
		Grafo grafo = new Grafo();

		// tercero agrego las vertices al grafo
		grafo.agregarVertice(buenosAires, caba, cordoba, santaFe, laPampa, rioNegro, chubut, santaCruz);

		// cuarto agrego las provincias limitrofes con sus respectivas similaridades
		grafo.agregarArista(buenosAires, caba, 0.2);
		grafo.agregarArista(buenosAires, cordoba, 0.5);
		grafo.agregarArista(buenosAires, santaFe, 0.5);
		grafo.agregarArista(buenosAires, rioNegro, 1.0);
		grafo.agregarArista(cordoba, santaFe, 0.5);
		grafo.agregarArista(laPampa, buenosAires, 1.5);
		grafo.agregarArista(laPampa, cordoba, 1.5);
		grafo.agregarArista(rioNegro, laPampa, 2.0);
		grafo.agregarArista(santaCruz, rioNegro, 0.5);
		grafo.agregarArista(santaCruz, chubut, 0.5);

		assertTrue(BFS.esConexo(grafo));
		Set<Vertice> ret =  BFS.alcanzablesDesdeUnVertice(grafo, buenosAires);
		assertTrue(ret.size()==8);
	}
}
