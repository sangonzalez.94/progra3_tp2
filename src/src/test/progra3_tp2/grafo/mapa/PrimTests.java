package src.test.progra3_tp2.grafo.mapa;
import static org.junit.Assert.*;


import org.junit.Test;

import progra3_tp2.model.grafo.mapa.AlgoritmoDePrim;
import progra3_tp2.model.grafo.mapa.Grafo;
import progra3_tp2.model.grafo.mapa.Vertice;
public class PrimTests {

	
	@Test
	public void casoExitoso() {
		//Creo grafo
		Grafo grafo = new Grafo();
		//Cargo las vertices
		Vertice vBuenosAires = new Vertice ("Buenos Aires");
		Vertice vCaba = new Vertice ("CABA");
		Vertice vEntreRios = new Vertice ("Entre Rios");
		Vertice vCordoba = new Vertice ("Cordoba");
		Vertice vSantaFe = new Vertice ("Santa Fe");
		Vertice vLaPampa = new Vertice ("La Pampa");
		Vertice vRioNegro = new Vertice ("Rio Negro");
		grafo.agregarVertice(vBuenosAires, vCaba, vEntreRios, vCordoba, vSantaFe, vLaPampa, vRioNegro);
		
		//Cargo las aristas
		grafo.agregarArista(vBuenosAires, vCaba, 0.0);
		grafo.agregarArista(vBuenosAires, vEntreRios, 1.0);
		grafo.agregarArista(vBuenosAires, vLaPampa, 2.0);
		grafo.agregarArista(vBuenosAires, vRioNegro, 2.0);
		grafo.agregarArista(vBuenosAires, vSantaFe, 0.5);
		grafo.agregarArista(vBuenosAires, vCordoba, 1.0);
		grafo.agregarArista(vLaPampa, vCordoba, 0.75);
		grafo.agregarArista(vLaPampa, vRioNegro, 1.0);
		grafo.agregarArista(vSantaFe, vEntreRios, 0.25);
		grafo.agregarArista(vSantaFe, vCordoba, 0.75);
		
		Grafo arbolMinimo = AlgoritmoDePrim.obtenerArbolGeneradorMinimo(grafo);
		
		assertTrue(arbolMinimo.tamano()==7);
		assertTrue(arbolMinimo.aristasDeUnVertice(vCaba).size()==1);
		assertTrue(arbolMinimo.aristasDeUnVertice(vBuenosAires).size()==2);
		assertTrue(arbolMinimo.aristasDeUnVertice(vSantaFe).size()==3);
		assertTrue(arbolMinimo.aristasDeUnVertice(vCordoba).size()==2);
		assertTrue(arbolMinimo.aristasDeUnVertice(vLaPampa).size()==2);
		assertTrue(arbolMinimo.aristasDeUnVertice(vRioNegro).size()==1);
		
		
		assertTrue(arbolMinimo.vecinosDeVertice(vCaba).contains(vBuenosAires));
		
		assertFalse(arbolMinimo.vecinosDeVertice(vBuenosAires).contains(vLaPampa));
		assertFalse(arbolMinimo.vecinosDeVertice(vBuenosAires).contains(vCordoba));
	}
}
